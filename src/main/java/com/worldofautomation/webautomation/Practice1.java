package com.worldofautomation.webautomation;


import org.testng.Assert;
import org.testng.annotations.Test;

public class Practice1 extends TestBase {

    // all test methods are public void , non static

    // write 50 test cases
    // 40 needs to be passing
    // 10 needs to be you couldn't solve
    // demo your works 15 mins each


    @Test
    public void userBeingAbleToLoginUsingValidCredentials() {
        launchBrowser("https://www.facebook.com", "chrome", false);
        waitFor(3);
        sendKeysUsingId("email", "testuser@gmail.com");
        sendKeysUsingId("pass", "testpassword");
        clickById("u_0_b");
        waitFor(3);
        closeBrowser();
    }

    @Test
    public void userBeingAbleToClickOnTermsAndConditions() {
        launchBrowser("https://www.facebook.com", "chrome", false);
        clickById("terms-link");
        waitFor(3);
        closeBrowser();
    }

    // validate user not facebook.com anymore
    // validate user went to messenger.com
    // validate "be together whenever" is displayed

    @Test
    public void userBeingAbleToClickOnMessenger() {
        launchBrowser("https://www.facebook.com", "chrome", false);
        clickByXpath("//a[@href='https://messenger.com/']");

        //waitFor(3);
        String url = getCurrentUrl();

        // * full url has been changed
        // -->validate user not facebook.com anymore
        if (url.contains("www.facebook.com")) {
            System.out.println("url is still at facebook.com");
        } else {
            System.out.println("url is not at facebook.com");
        }

        // -->validate user went to messanger.com
        if (url.contains("messenger.com")) {
            System.out.println("messenger url has been validated");
        } else {
            System.out.println("messenger url has not been validated");
        }

        // * part of the url has been changed
        /*if(url.contains("lite")){
            System.out.println("url has been validated ");
        }else {
            System.out.println("url has not been validated ");
        }*/

        System.out.println(url);

        //--> validate "be together whenever" is displayed

        //-->>> //h1[@class='_6b6b']

        //--> (//h1[@class='_6b6b'])[1]
        boolean isTogetherDisplayed = validateXpathIsDisplayed("(//h1[@class='_6b6b'])[1]");
        System.out.println("be together whenever is displayed : " + isTogetherDisplayed);

        waitFor(3);
        closeBrowser();
    }


    @Test
    public void userBeingAbleToClickOnMessengerWithAssertion() {
        launchBrowser("https://www.facebook.com", "chrome", false);
        clickByXpath("//a[@href='https://messenger.com/']");
        waitFor(3);
        String url = getCurrentUrl();

        Assert.assertFalse(url.contains("www.facebook.com"), "url is at facebook.com");
        Assert.assertTrue(url.contains("www.messenger.com"), "url is not at messenger.com");

        boolean isTogetherDisplayed = validateXpathIsDisplayed("(//h1[@class='_6b6b'])[1]");
        Assert.assertTrue(isTogetherDisplayed, " be together whenever is not displayed");
        //Assert.assertEquals(isTogetherDisplayed,true);

        String getdata = getTextFromXpath("(//h1[@class='_6b6b'])[1]");
        Assert.assertEquals(getdata, "Be together, when");

        waitFor(3);
        closeBrowser();
    }
}
