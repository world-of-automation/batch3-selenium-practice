package com.worldofautomation.webautomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumBasics2 {
    private static WebDriver driver = null;

    public static void main(String[] args) {
        launchBrowser("https://www.facebook.com", "chrome");
        waitFor(3);

        // fill username
        //email -->ID
        //driver.findElement(By.id("email")).sendKeys("testuser@gmail.com");
        sendKeysUsingId("email", "testuser@gmail.com");

        // fill pass
        //pass -->ID
        //driver.findElement(By.id("pass")).sendKeys("testpassword");
        sendKeysUsingId("pass", "testpassword");

        // click login
        //u_0_b -->ID
        //driver.findElement(By.id("u_0_b")).click();
        clickById("u_0_b");

        waitFor(3);
        closeBrowser();
    }

    public static void clickById(String id) {
        driver.findElement(By.id(id)).click();
    }

    public static void sendKeysUsingId(String id, String keysToSend) {
        driver.findElement(By.id(id)).sendKeys(keysToSend);
    }


    public static void launchBrowser(String url, String browserName) {
        if (browserName.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
            driver = new ChromeDriver();
        } else if (browserName.equalsIgnoreCase("mozilla")) {
            System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
            driver = new FirefoxDriver();
        }
        driver.get(url);
    }

    public static void closeBrowser() {
        driver.close();
    }

    public static void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //input[@type='email']

    //doubleSlash tagName[@propertiesName='propertiesValue']
    //id
    //name
    //aria-label
    //others
    //class


}
