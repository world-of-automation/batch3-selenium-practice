package com.worldofautomation.webautomation;

import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumBasics1_Mozilla {

    public static void main(String[] args) {
        // IllegalStateException --> setup webdriver.chrome.driver in system property
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");

        // obj for chromedriver
        FirefoxDriver driver = new FirefoxDriver();

        // url to navigate to
        driver.get("https://www.facebook.com");

        // wait for 5 secs
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // closing the browser
        driver.close();
    }
}
