package com.worldofautomation.webautomation;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Practice4 extends TestBase {

    @BeforeMethod
    public void startBeforeEveryTest() {
        launchBrowser("https://www.ebay.com", "chrome", true);
        waitFor(3);
    }

    @AfterMethod
    public void closeAfterEveryTest() {
        waitFor(30);
        closeBrowser();
    }


    // homework to make these methods parameterized

    @Test
    public void testScrolling() {
        WebElement reg = getDriver().findElement(By.linkText("Registration"));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView(true);", reg);
    }


    @Test
    public void typeUsingJS() {
        WebElement searchType = getDriver().findElement(By.id("gh-ac"));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].value='Java Books'", searchType);
    }


    @Test
    public void clickUsingJS() {
        WebElement signIn = getDriver().findElement(By.linkText("Sign in"));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", signIn);
    }


}
