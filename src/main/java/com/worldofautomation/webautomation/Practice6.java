package com.worldofautomation.webautomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class Practice6 extends TestBase {

    @BeforeMethod
    public void startBeforeEveryTest() {
        launchBrowser("https://www.aa.com", "chrome", false);
        waitFor(3);
    }

    @AfterMethod
    public void closeAfterEveryTest() {
        waitFor(10);
        closeBrowser();
    }

    @Test
    public void validateUserIsSelectedRoundTripByDefault() {

        List<WebElement> elementList = getDriver().findElements(By.xpath("//input[@type='radio']"));

        for (int i = 0; i < elementList.size(); i++) {
            if (elementList.get(i).isSelected() && elementList.get(i).isDisplayed()
                    && elementList.get(i).isEnabled()
                    && elementList.get(i).getAttribute("value").equalsIgnoreCase("roundTrip")) {

                System.out.println("Validatd : " + elementList.get(i).getAttribute("value"));
            }
        }
    }

    @Test
    public void validateUserIsSelectedFlightByDefault() {
        List<WebElement> elementList = getDriver().findElements(By.xpath("//input[@type='radio']"));

        for (int i = 0; i < elementList.size(); i++) {
            if (elementList.get(i).isSelected()
                    && elementList.get(i).getAttribute("value").equalsIgnoreCase("flight")) {

                System.out.println("Validatd : " + elementList.get(i).getAttribute("value"));
            }
        }
    }


    @Test
    public void clickOnOneWay() {
        getDriver().findElement(By.xpath("//label[@for='flightSearchForm.tripType.oneWay']")).click();
        List<WebElement> elementList = getDriver().findElements(By.xpath("//input[@type='radio']"));
        for (int i = 0; i < elementList.size(); i++) {
            if (elementList.get(i).isSelected()
                    && elementList.get(i).getAttribute("value").equalsIgnoreCase("oneWay")) {

                System.out.println("Validatd : " + elementList.get(i).getAttribute("value"));
            }
        }
    }

}
