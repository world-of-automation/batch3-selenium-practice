package com.worldofautomation.webautomation;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Practice7 extends TestBase {

    @BeforeMethod
    public void startBeforeEveryTest() {
        launchBrowser("https://forms.pnc.com/content/pnc-bbdda-forms/en/business-checking.html", "chrome", false);
        waitFor(3);
    }

    @AfterMethod
    public void closeAfterEveryTest() {
        waitFor(10);
        closeBrowser();
    }

    @Test
    public void validateUserIsSelectedIfUSC() {
        getDriver().findElement(By.xpath("//input[@aria-label='Business Zip Code']")).sendKeys("11374");
        waitFor(3);
        getDriver().findElement(By.xpath("//button[@aria-label='START']")).click();

        //(//label[text()='Are you a U.S citizen?']/..)/following-sibling::div//label[text()='No']

    }

}
