package com.worldofautomation.webautomation;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class Practice3 extends TestBase {

    @BeforeMethod
    public void startBeforeEveryTest() {
        launchBrowser("https://www.ebay.com", "chrome", false);
        waitFor(3);
    }

    @AfterMethod
    public void closeAfterEveryTest() {
        waitFor(3);
        closeBrowser();
    }

    @Test
    public void userBeingAbleToView36OptionsFromDropDown() {
        List<WebElement> dropdownList = getDriver().findElements(By.xpath("//select[@id='gh-cat']/option"));
        System.out.println(dropdownList.size());
        Assert.assertEquals(dropdownList.size(), 36);

        ArrayList<String> actualList = new ArrayList<>();

        for (int i = 0; i < dropdownList.size(); i++) {
            System.out.println(dropdownList.get(i).getText());
            actualList.add(dropdownList.get(i).getText());
        }

        ArrayList<String> expectedList = new ArrayList<>();
        expectedList.add("All Categories");
        expectedList.add("Antiques");
        // to do
        Assert.assertEquals(actualList, expectedList);
    }


    @Test
    public void validateUserBeingAbleToChooseCameraFromElectronicsMouseHover() {
        Actions actions = new Actions(getDriver());
        WebElement electronics = getDriver().findElement(By.linkText("Motors"));
        actions.moveToElement(electronics).build().perform();

        //waitFor(2); //mandatory
        // explicit wait --> waiting against condition
        WebElement cameraNphotos = getDriver().findElement(By.linkText("Classics"));

        WebDriverWait webDriverWait = new WebDriverWait(getDriver(), 20);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(cameraNphotos));


        cameraNphotos.click();
    }

    //      (//a[text()='Motors'])[2]/following-sibling::div[2]//li


}