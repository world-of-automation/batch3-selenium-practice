package com.worldofautomation.webautomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Practice5SeleniumMethods extends TestBase {


    @Test
    public void testUsingDragNdrop() {
        launchBrowser("http://demo.guru99.com/test/drag_drop.html", "chrome", false);

        WebElement from = getDriver().findElement(By.xpath("(//li[@data-id='2'])[2]"));
        WebElement to = getDriver().findElement(By.xpath("//ol[@id='amt8']"));

        Actions actions = new Actions(getDriver());
        actions.dragAndDrop(from, to).build().perform();

        waitFor(5);
        closeBrowser();
    }

    @Test
    public void testUsingiFrame() {
        launchBrowser("https://demoqa.com/iframe-practice-page/", "chrome", false);

        getDriver().switchTo().frame("IF2");
        // frame id, name, index
        waitFor(5);
        WebElement element = getDriver().findElement(By.linkText("Sortable"));

        Assert.assertTrue(element.isDisplayed());
        element.click();

        getDriver().switchTo().defaultContent();

        waitFor(5);
        closeBrowser();
    }


    @Test
    public void testUsingAlert() {
        launchBrowser("http://demo.guru99.com/test/delete_customer.php", "chrome", false);

        getDriver().findElement(By.xpath("//input[@name='cusid']")).sendKeys("1");
        getDriver().findElement(By.xpath("//input[@name='submit']")).click();

        waitFor(5);
        String alertText = getDriver().switchTo().alert().getText();
        System.out.println(alertText);
        waitFor(5);

        getDriver().switchTo().alert().accept();

        //getDriver().switchTo().alert().dismiss();
        waitFor(5);
        String alertText2 = getDriver().switchTo().alert().getText();
        System.out.println(alertText2);
        waitFor(5);
        closeBrowser();
    }


}
