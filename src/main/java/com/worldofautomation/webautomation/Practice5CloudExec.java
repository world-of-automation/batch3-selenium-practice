package com.worldofautomation.webautomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Practice5CloudExec {


    @Test
    public void userBeingAbleToView36OptionsFromDropDown() throws MalformedURLException {

        String bsUserName = "azharkautomation1";
        String bsAccessKey = "mFTz1sSxLoaBGPBJpFPQ";
        String url = "https://" + bsUserName + ":" + bsAccessKey + "@hub-cloud.browserstack.com/wd/hub";

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "80.0");
        caps.setCapability("os", "OS X");
        caps.setCapability("os_version", "Catalina");
        caps.setCapability("resolution", "1024x768");
        caps.setCapability("name", "Bstack-[Java] Sample Test");

        WebDriver driver = new RemoteWebDriver(new URL(url), caps);

        driver.get("https://www.ebay.com");
        List<WebElement> dropdownList = driver.findElements(By.xpath("//select[@id='gh-cat']/option"));

        System.out.println(dropdownList.size());
        Assert.assertEquals(dropdownList.size(), 36);

        ArrayList<String> actualList = new ArrayList<>();

        for (int i = 0; i < dropdownList.size(); i++) {
            System.out.println(dropdownList.get(i).getText());
            actualList.add(dropdownList.get(i).getText());
        }

        ArrayList<String> expectedList = new ArrayList<>();
        expectedList.add("All Categories");
        expectedList.add("Antiques");
        Assert.assertEquals(actualList, expectedList);
    }

}
