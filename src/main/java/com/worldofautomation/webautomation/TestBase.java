package com.worldofautomation.webautomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestBase {

    private static WebDriver driver = null;

    public static WebDriver getDriver() {
        return driver;
    }

    public static void clickByXpath(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    public static void clickById(String id) {
        driver.findElement(By.id(id)).click();
    }

    public static boolean validateXpathIsDisplayed(String xpath) {
       /* WebElement element = driver.findElement(By.xpath(xpath));
        boolean isDisplayed = element.isDisplayed();*/

        boolean ifDisplayed = driver.findElement(By.xpath(xpath)).isDisplayed();
        return ifDisplayed;
    }

    public static boolean validateListOfXpathIsDisplayed(String xpath) {
        List<WebElement> elementList = driver.findElements(By.xpath(xpath));
        boolean flag = false;

        for (int i = 0; i < elementList.size(); i++) {

            // if displayed will throw exception if the element is not the in the page
            // no such element found exeption

            if (elementList.get(i).isDisplayed()) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static void sendKeysUsingId(String id, String keysToSend) {
        driver.findElement(By.id(id)).sendKeys(keysToSend);
    }

    public static void launchBrowser(String url, String browserName, boolean cloud) {
        if (cloud == true) {
            String bsUserName = "azharkautomation1";
            String bsAccessKey = "mFTz1sSxLoaBGPBJpFPQ";
            String urlForBS = "https://" + bsUserName + ":" + bsAccessKey + "@hub-cloud.browserstack.com/wd/hub";

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "Chrome");
            caps.setCapability("browser_version", "80.0");
            caps.setCapability("os", "OS X");
            caps.setCapability("os_version", "Catalina");
            caps.setCapability("resolution", "1024x768");
            caps.setCapability("name", "Bstack-[Java] Sample Test");

            URL urll = null;
            try {
                urll = new URL(urlForBS);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            driver = new RemoteWebDriver(urll, caps);
        } else {
            if (browserName.equalsIgnoreCase("chrome")) {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                driver = new ChromeDriver();
            } else if (browserName.equalsIgnoreCase("mozilla")) {
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
                driver = new FirefoxDriver();
            }
        }
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
    }

    public static void closeBrowser() {
        //driver.close();
        driver.quit();
    }

    public static void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void validateIfTextIsAsExpected(String xpath, String expectedText) {
        WebElement element = driver.findElement(By.xpath(xpath));
        String actualText = element.getText();
        Assert.assertEquals(actualText, expectedText);
    }

    public String getTextFromXpath(String xpath) {
        String text = driver.findElement(By.xpath(xpath)).getText();
        return text;
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

}
