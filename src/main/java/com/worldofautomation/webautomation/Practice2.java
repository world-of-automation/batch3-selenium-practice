package com.worldofautomation.webautomation;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Practice2 extends TestBase {

    @BeforeMethod
    public void startBeforeEveryTest() {
        launchBrowser("https://www.barnesandnoble.com", "chrome", false);
        waitFor(3);
    }

    @AfterMethod
    public void closeAfterEveryTest() {
        waitFor(3);
        closeBrowser();
    }

    @Test
    public void userBeingAbleToLoginUsingValidCredentials() {
        boolean displayed = validateListOfXpathIsDisplayed("//div[@class='hp-quote-bg bg']");
        System.out.println(displayed);
    }

    @Test
    public void userBeingAbleToSeeAdvertise() {
        validateIfTextIsAsExpected("//a[@href='https://www.barnesandnoble.com/h/bn-advertising']", "Advertise");
    }

    //xpath using text
    //tagName[text()='_____value_____']
    //tagName[contains(text(),'_____partialValue_____')]

}