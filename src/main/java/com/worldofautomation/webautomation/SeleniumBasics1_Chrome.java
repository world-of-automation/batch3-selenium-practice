package com.worldofautomation.webautomation;

import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumBasics1_Chrome {

    public static void main(String[] args) {
        // IllegalStateException --> setup webdriver.chrome.driver in system property
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        // obj for chromedriver
        ChromeDriver driver = new ChromeDriver();
        // url to navigate to
        driver.get("https://www.facebook.com");


        // wait for 5 secs
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // closing the browser
        driver.close();
    }
}
