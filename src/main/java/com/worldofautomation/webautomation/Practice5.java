package com.worldofautomation.webautomation;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class Practice5 extends TestBase {

    public static void switchToTab(int tabNo) {
        ArrayList<String> tabList = new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabList.get(tabNo));
    }

    @BeforeMethod
    public void startBeforeEveryTest() {
        launchBrowser("https://www.curryheights.com", "chrome", false);
        waitFor(3);
    }

    @AfterMethod
    public void closeAfterEveryTest() {
        waitFor(10);
        closeBrowser();
    }

    @Test
    public void testTabHandler() {
        getDriver().findElement(By.linkText("Catering")).click();

        switchToTab(1);
        waitFor(5);
        switchToTab(0);
        waitFor(5);
        switchToTab(1);
        waitFor(5);
        getDriver().close();
        waitFor(5);
        //boolean validate = validateXpathIsDisplayed("//h1[@data-reactid='6']");
        //Assert.assertTrue(validate);
    }

}
